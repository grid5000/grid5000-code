#!/bin/bash -e

IFS=$'\n'
UNIT='s'
MNTDEST=/mnt/dest

VMLINUZ=/vmlinuz
INITRD=/initrd.img

function do_parted()
{
	arr=("${!1}")
	cmds="unit\n${UNIT}\n"
	for elem in "${arr[@]}"
	do
		cmds="${cmds}${elem}\n"
	done
	cmds="${cmds}quit\n"
	echo -n -e $cmds | parted $PARTED_OPTS $DEV
}

if [ $# -lt 3 ]
then
	echo "usage: $0 <device> <running_part> <part_to_format>"
	exit 1
fi

if [ -z "$(which parted)" ]
then
	echo "parted is not installed"
	exit 1
fi

DEV=$1
DEV_escaped=$(echo $DEV | sed 's/\//\\\//g')
OLD=$2
NEW=$3


echo ''
echo 'Step1: Formating the partition'

echo '  Gather partitioning information'

cmd=('print')

PARTED_OPTS='-anone'
parts=$(do_parted cmd[@] | grep '^\ *[0-9]\+\ *.*$')

oldpart=$(echo "$parts" | grep "^\ *${OLD}\ *.*$")
oldpart_fstype=$(echo "$oldpart" | awk '{print $6}')

echo "  Umount ${DEV}${NEW} partition"
umount -f ${DEV}${NEW} &>/dev/null

echo "  Formating new partition ${DEV}${NEW} using $oldpart_fstype"
mkfs -t $oldpart_fstype ${DEV}${NEW} &>/dev/null
sync


echo 'Step2: Migration of the partition'

echo "  Copy ${DEV}${OLD} to ${DEV}${NEW}"
mkdir -p $MNTDEST
mount ${DEV}${NEW} $MNTDEST
cd /
tar czf - $(ls / | grep -v "^\(dev\|proc\|sys\|mnt\)$") | (cd $MNTDEST; tar xzfp -)

echo "  Prepare filesystem"
mkdir -p ${MNTDEST}/proc
chmod 555 ${MNTDEST}/proc
#mount -t proc none ${MNTDEST}/proc
echo "    /proc ok"

mkdir -p ${MNTDEST}/sys
chmod 755 ${MNTDEST}/sys
#mount -t sysfs none ${MNTDEST}/sys
echo "    /sys ok"

mkdir -p ${MNTDEST}/dev
chmod 755 ${MNTDEST}/dev
#mount -o bind /dev ${MNTDEST}/dev
echo "    /dev ok"

mkdir -p ${MNTDEST}/dev/pts
chmod 755 ${MNTDEST}/dev/pts
#mount -t devpts none ${MNTDEST}/dev/pts
(cd ${MNTDEST}/dev; MAKEDEV generic)
echo "    /dev/pts ok"

cp /etc/fstab ${MNTDEST}/etc/fstab
sed "/${DEV_escaped}${NEW}/d" -i ${MNTDEST}/etc/fstab
sed "s/${DEV_escaped}${OLD}/${DEV_escaped}${NEW}/g" -i ${MNTDEST}/etc/fstab
echo "    /etc/fstab ok"
sync


echo 'Step3: Pivot root filesystem'

echo "  Mount ${DEV}${NEW}"
mount ${DEV}${NEW} $MNTDEST &>/dev/null

echo "  Kexec on ${DEV}${NEW}"
cmdline=$(cat /proc/cmdline | sed -e "s/${DEV_escaped}${OLD}/${DEV_escaped}${NEW}/g" -e 's/BOOT_IMAGE=[^ ]* //g' -e 's/ ro / rw /g' -e 's/ ro$/ rw/g' -e 's/^ro /rw /g')
echo s > /proc/sysrq-trigger
echo u > /proc/sysrq-trigger
kexec -l ${MNTDEST}/$(readlink -f $VMLINUZ) \
  --initrd="${MNTDEST}/$(readlink -f $INITRD)" \
  --append="$cmdline"
echo "  Kexec reboot in 3s"
sleep 1
echo "  Kexec reboot in 2s"
sleep 1
echo "  Kexec reboot in 1s"
sleep 1
echo "  reboot ..."

nohup kexec -e 1>/dev/null 2>/dev/null </dev/null &

