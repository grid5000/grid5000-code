#!/bin/bash

P=${0%/*}
cmd="run.sh"
T=$(< $P/target.conf)
W=5
J=${1:-0}
H=$(hostname -f)
S=${H#*.}
R=${TAKTUK_RANK:-0}

mkdir -p $P/res/$S $P/sta/$S

echo "R$R: tempo for $((R * W))s"
sleep $((R * W))
echo "R$R: Go..."

while true; do
	C=0
	m=$T
	D=$(date +%s)
	for c in $(< $P/params.conf); do
		r=$(cat $P/res/*/$c.* 2> /dev/null | wc -l)
		s=$(ls $P/sta/*/$c.* 2> /dev/null | wc -l)
		t=$((r+s))		
		echo -n "$c: $r results, $s running => $t out of $T"
		if [ $t -lt $m ]; then
			echo " <-"
			m=$t
			C=$c
		else
			echo
		fi
	done
	if [ $C -gt 0 ]; then
		echo "R$R: Compute $C"		
		s="$P/sta/$S/$C.$J.$H.$R.$D.$$"
		f="$P/res/$S/$C.$J.$H.$R.$D.$$"
		touch $s
		$P/$cmd $C > $f
		rm -f $s
	else
		echo "=> Looks like we are done !"
		exit 0
	fi
done
