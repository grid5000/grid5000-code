#!/bin/bash

W=${1:-300}
h=$(hostname -f)
s=${h#*.}
P=${0%/*}
T=$(< $P/target.conf)
cd $P
p=$PWD

while true; do
	echo -n "#### Cleaning up any old stamps from old jobs."
	mkdir -p $P/sta/$s
	for j in $(cd $P/sta/$s && ls *.* 2>/dev/null | cut -f2 -d. | sort -u); do
		if [ $j -gt 0 ] && oarstat -j $j -s | grep -q -e "Terminated" -e "Error"; then 
			echo -n "$j"
			rm -f $P/sta/$s/*.$j.*
		else 
			echo -n "."
		fi
	done
	echo " Done."
	echo

	echo "#### Sync'ing data with all sites..."
	for r in $(< $P/sites.conf); do
		if [ "$r.grid5000.fr" != $s ]; then 
			echo "=> $r... "
			rsync -av --delete $P/res/$s $r:$p/res/$s
			rsync -av --delete $P/res/$s $r:$p/res/$s
			echo
		fi
	done

	echo "#### Sleep for $W seconds..."
	sleep $W
done


