#!/bin/bash

P=${0%/*}
T=$(< $P/target.conf)
C=0
for c in $(< $P/params.conf); do
	r=$(cat $P/res/*/$c.* 2> /dev/null | wc -l)
	s=$(ls $P/sta/*/$c.* 2> /dev/null | wc -l)
	t=$((r+s))		
	echo -n "$c: $r results, $s running (total $t)"
	if [ $t -lt $T ]; then
		echo "-> currently selected"
		T=$t
		C=$c
	else
		echo
	fi
done
if [ $C -gt 0 ]; then
	echo "=> Will compute $C"		
else
	echo "=> Looks like we are done !"
fi

