This framework is intended to help running multi-parametric jobs on Grid'5000.
Its particularity is that it features a very simple computation load balancing
algorithm, guarantying to always have quite the same # of results F(X) for all
X.  In this, it can actually complete the use of CiGri3, but was developed as a
standalone framework.

All files are:
* Copyright: 2012, Pierre Neyron, Laboratoire d'Informatique de Grenoble (CNRS)
* License: APACHE
* Contact: pierre.neyron@imag.fr

*****************
*** Hypethesis: *
*****************
We have
- a function F
- a set of X to evaluate with F
=> want to gather the result of F(X) T times.
We also want results to be well balanced, i.e. try to always have quite the same # of F(X) for all X, even before reaching T.

F in defined in ./run.sh
The X values are listed in a raw in ./params.conf, separated by spaces.
T is an integer defined in ./target
./sites.conf is the list of the Grid'5000 sites we can use.

./fx is an example of program. Here F(X) is the output of ./fx X 1 (see ./run.sh). Due to licensing concerns, it is not shipped with the framework.

************
*** Usage: *
********** *
One should use the framework as follows:

1/ Prepare the sites
Untar the framework on on site. A ~/mp directory should be present.
Adapt run.sh, params.conf, target.conf, sites.conf .
run ./push_scripts_and_conf_to_other_sites.sh to populate the other sites. (Run it again if you change something)

2/ Make sure all sites stay in sync and clean
Run ./sync_and_clean.sh on all sites in a screen for instance, and let it live (it must run like a deamon).

3/ Submit jobs
Submit jobs on sites according to the resources availability (look at Grid'5000 monitoring tools):
Run: ./submit_job.sh <#core> as many time as you want, on any sites you want (which is configured in ./sites.conf).
Rq:
- Try to not overload the sites with to many jobs (anyway, there is a limite of 50 jobs per sites in Grid'5000).
- use 8 or 16 or... or 64 cores per job for instance, depending on the resource availability on the site.
- Mind the fact that we use besteffort jobs. The bigger your jobs are (in # of cores), the higher the chances that OAR needs
to kill one of your jobs to give place to another user.

4/ Watch progress
You can follow the progress at anytime using:
./show_status.sh

5/ Get the results
On any of the sites, you can list the results (all of them) as follows:
cat ./res/*/*

****************
*** Internals: *
****************
X is called C in the scripts.

./choose_and_run_one.sh is the unitary script that run on 1 core.
It is called by ./taktuk_cmd.sh .
./taktuk_cmd.sh is used in the oarsub command, as scripted in sub.sh

./sync_and_clean.sh is a standalone script, but must run to ensure that all sites have the same view of the progress of the work
while the jobs are running.

./show_status.sh is also a standalone script.

./push_scripts_and_conf_to_other_sites.sh is also a standalone script.

./sim_choice.sh tries to show what would be the next X -> F(X) to be computed, with regard to the computation balancing algorithm.

./res/<sites>/ contains the results, a new file is created there for every new core computing a F(X)

./sta/<sites>/ contains the stamps. To one core being computing a F(X) corresponds a stamp file in the directory.
This stamp file is supposed to be deleted once the corresponding F(X) computed. However, since we are using besteffort jobs, some
"zombies" may remain. They are eventually deleted by ./sync_and_clean.sh or ./show_status.sh or ./taktuk_cmd.sh.

