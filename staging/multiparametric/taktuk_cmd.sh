#!/bin/bash

P=${0%/*}
h=$(hostname -f)
s=${h#*.}
echo "*** Start ***"
echo "* Remove any old stamp..."
mkdir -p $P/sta/$s
for j in $(cd $P/sta/$s && ls *.*.* | cut -f2 -d. | sort -u); do
	if [ $j -gt 0 ] && ssh frontend "oarstat -j $j -s" | grep -q -e "Terminated" -e "Error"; then 
		echo "Job $j is not running anymore -> removing stamps"
		rm -v $P/sta/$s/*.$j.*
	else
		echo "Job $j is running"
	fi
done

echo "* Spawning work on nodes..."
taktuk -s -c oarsh -f $OAR_NODEFILE b e [ $P/choose_and_run_one.sh $OAR_JOBID ]
echo "*** End ***"
