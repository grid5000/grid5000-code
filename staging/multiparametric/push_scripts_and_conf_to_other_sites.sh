#!/bin/bash

h=$(hostname -f)
s=${h#*.}
P=${0%/*}
cd $P
p=$PWD

echo "Pushing script and conf to other sites..."
for r in $(< $P/sites.conf); do
	if [ "$r.grid5000.fr" != $s ]; then 
		echo "=> $r... "
		ssh $r "mkdir -p $p/res $p/sta"
		rsync -av fx *.sh *.conf $r:$p/
	fi
done


