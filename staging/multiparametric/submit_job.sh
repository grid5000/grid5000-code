#!/bin/bash

CORE=${1:-16}
W=10
P=${0%/*}
oarsub -l $core=$CORE,walltime=$W  -t idempotent -t besteffort $P/taktuk_cmd.sh
