#!/bin/bash

P=${0%/*}
T=$(< $P/target.conf)
h=$(hostname -f)
s=${h#*.}

echo -n "Cleaning up any old stamps from old jobs."
mkdir -p $P/sta/$s
for j in $(cd $P/sta/$s && ls *.* 2>/dev/null | cut -f2 -d. | sort -u); do
	if [ $j -gt 0 ] && oarstat -j $j -s | grep -q -e "Terminated" -e "Error"; then 
		echo -n "$j"
		rm -f $P/sta/$s/*.$j.*
	else 
		echo -n "."
	fi
done
echo " Done."

echo
echo -n "Local running jobs: "
ls $P/sta/$s/*.*.* 2>/dev/null | wc -l
echo -n "Total running jobs: "
ls $P/sta/*/*.*.* 2>/dev/null | wc -l

for s in $(< sites.conf); do
	echo "*** $s:"
	ssh $s "oarstat | grep pneyron"
done 

echo
echo "Current status:"
rr=0
tt=0
TT=0
for c in $(< $P/params.conf); do
	r=$(cat $P/res/*/$c.* 2> /dev/null | wc -l)
	s=$(ls $P/sta/*/$c.* 2> /dev/null | wc -l)
	t=$((r+s))		
	echo "$c: $r results [$(($r*100/$T))%], $s running => total $t [$(($t*100/$T))%]"
	((rr+=r))
	((tt+=t))
	((TT+=T))
done
echo "Total: $rr results [$(($rr*100/$TT))%], $tt pending [$(($tt*100/$TT))%]"

