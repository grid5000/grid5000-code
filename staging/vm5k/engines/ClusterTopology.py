from vm5k_engine import *
from execo_g5k import get_g5k_clusters

class ClusterTopology( Engine ):
    
    def __init__(self):
        super(ClusterTopology, self).__init__()
        
    def run(self):
        clusters = get_g5k_clusters()
        for cluster in clusters:
            print cluster
        
