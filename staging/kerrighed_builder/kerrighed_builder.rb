#!/usr/bin/ruby
#
# kerrighed_builder.rb
#    Little script based on Kadeploy 3 to automatically deploy a 
#    Kerrighed cluster. It performs the following operations:
#      * deployment of a Debian environment
#      * configuration of a master node to serve as a NFSROOT server
#      * download and build of a Kerrighed kernel
#      * configuration of the Kerrighed environment
#      * boot of the slave nodes
#
#    The whole operation requires about 15 minutes, depending on the
#    hardware used.
########
# Usage
#    Requirements:
#      * a list of nodes (the first on will be considered as the master
#                         and the other will be the slaves)
#      * a kernel config file configured for Kerrighed on your hardware
#      * a password less SSH private key
#      * a Debian based environment recorded in the kadeploy database
#      * Kadeploy 3.1-3 or higher
#    Examples of use:
#      * ruby kerrighed_builder.rb -f nodes -e lenny-base -k /home/ejeanvoine/.ssh/id_dsa -c config-2.6.30-krg -s --http-proxy "http://proxy:3128"
#      * ruby kerrighed_builder.rb -m master_node -f slave_nodes -e lenny-base -k /home/ejeanvoine/.ssh/id_dsa -c config-2.6.30-krg -d
#      * ruby kerrighed_builder.rb -f nodes -e lenny-krg -k /home/ejeanvoine/.ssh/id_dsa -s --bypass-master-install
########
# Configuration
#    Kerrighed session:
#      Kerrighed uses a session system to allow the cohabitation of
#      several Kerrighed clusters on the same LAN. So you have to
#      ensure that the kerrighed session you use is only used by your
#      cluster.
#   Ethernet device:
#      This value allows to specify the ethernet device to use.
#   Serial console speed:
#      The console speed option allows to specify the speed of the
#      serial console. This is useful if you plan to use kaconsole3.
########
# Licence
#
# Copyright (C) 2010 Emmanuel Jeanvoine <emmanuel.jeanvoine@inria.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
########

require "optparse"
require "ostruct"
require "resolv"
require "ping"


#KERRIGHED_URL = "http://gforge.inria.fr/frs/download.php/27161/kerrighed-3.0.0.tar.gz"
#KERRIGHED_URL = "http://www.loria.fr/~ejeanvoi/kerrighed-3.0.0.tar.gz"
KERRIGHED_URL = "http://public.rennes.grid5000.fr/~ejeanvoine/kerrighed-3.0.0.tar.gz"
#KERNEL_URL = "http://www.kernel.org/pub/linux/kernel/v2.6/linux-2.6.30.tar.bz2"
#KERNEL_URL = "http://www.loria.fr/~ejeanvoi/linux-2.6.30.tar.bz2"
KERNEL_URL = "http://public.rennes.grid5000.fr/~ejeanvoine/linux-2.6.30.tar.bz2"

KADEPLOY_CMD = "kadeploy3"
KAREBOOT_CMD = "kareboot3"

SSH_BASE_CONNECTOR = "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o PreferredAuthentications=publickey -o BatchMode=yes"
SCP_BASE_CONNECTOR = "scp -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o PreferredAuthentications=publickey -o BatchMode=yes"
DEBIAN_PACKAGES = "dhcp3-common nfs-common nfsbooted openssh-server automake autoconf libtool pkg-config gawk rsync bzip2 libncurses5 libncurses5-dev lsb-release xmlto patchutils build-essential udev"
NFS_EXPORT_DIR = "/NFSROOT/kerrighed"

class Command
  @debug = nil
  @ssh_connector = nil
  @scp_connector = nil
  @host = nil
  @silent = nil
 
  def initialize(debug, ssh_connector, scp_connector, host, silent)
    @debug = debug
    @ssh_connector = ssh_connector
    @scp_connector = scp_connector
    @host = host
    @silent = silent
  end
  
  def run_local(cmd)
    puts cmd if @debug
    cmd += " 1>/dev/null" if @silent
    if not system(cmd) then
      puts "ERROR !!!"
        exit(1)
    end
  end

  def run_distant(cmd)
    run_local("#{@ssh_connector} root@#{@host} \"#{cmd}\"")
  end

  def send(file, dest)
    run_local("#{@scp_connector} #{file} root@#{@host}:#{dest}")
  end

  def get(file, dest)
    run_local("#{@scp_connector} root@#{@host}:#{file} #{dest}")
  end
end

def log(msg)
  puts "\n..:: #{msg} ::..\n"
end

def parse_opts()
  config = OpenStruct.new
  config.master = String.new
  config.slaves = Array.new
  config.environment = String.new
  config.debug = false
  config.key = String.new
  config.config_file = String.new
  config.silent = false
  config.console_speed = "19200"
  config.eth_device = "eth0"
  config.kerrighed_session = "10"
  config.cpu_nb = 4
  config.bypass_master_install = false
  config.http_proxy = String.new

  opts = OptionParser::new { |opt|
    opt.summary_indent = "  "
    opt.summary_width = 32
    opt.banner = "Usage: kerrighed_builder.rb [options]"
    opt.separator "Options:"
    opt.on("-b", "--bypass-master-install", "Bypass the master node installation") {
      config.bypass_master_install = true
    }
    opt.on("-c", "--config-file FILE", "Kerrighed kernel config file") { |f|
      config.config_file = f
    }
    opt.on("--console-speed SPEED", "Set the serial console speed") { |s|
      config.console_speed = s
    }
    opt.on("--cpu NB", "Number of CPU (used for compilation") { |n|
      config.cpu_nb = n.to_i
    }
    opt.on("-d", "--debug", "Activate debug mode") {
      config.debug = true
    }
    opt.on("-e", "--environment ENVNAME", "Environment to deploy on the master node") { |e|
      config.environment = e
    }
    opt.on("--eth-device DEV", "Set the ethernet device to use") { |d|
      config.eth_device = d
    }
    opt.on("-f", "--nodes-file FILE", "Nodes file") { |f|
      list = IO.readlines(f)
      config.master = list[0].chomp
      (1..(list.length - 1)).each { |i|
        config.slaves.push(list[i].chomp) if list[i] != "\n"
      }
    }
    opt.on("--http-proxy PROXY", "Specify an HTTP proxy for the Debootstrap") { |p|
      config.http_proxy = p
    }
    opt.on("-k", "--key KEY", "Password less private key") { |k|
      config.key = k
    }
    opt.on("--kerrighed-session ID", "Set the Kerrighed session ID") { |i|
      config.kerrighed_session = i
    }
    opt.on("-s", "--silent", "Turn on silent mode") {
      config.silent = true
    }
  }
  begin
    opts.parse!(ARGV)
  rescue 
    puts "Option parsing error: #{$!}"
    return nil
  end
  return config
end

config = parse_opts()

exit(1) if config == nil

command = Command.new(config.debug,
                      SSH_BASE_CONNECTOR + " -i #{config.key}", 
                      SCP_BASE_CONNECTOR + " -i #{config.key}",
                      config.master,
                      config.silent)

dns = Resolv::DNS.new
master_ip = dns.getaddress(config.master).to_s
dns.close

log("Launching Kadeploy on #{config.master} with #{config.environment} environment")
cmd = "#{KADEPLOY_CMD} -e #{config.environment} -m #{config.master} -k #{config.key}.pub --verbose-level 0"
command.run_local(cmd)

if (not config.bypass_master_install) then
  log("Installing missing packages")
  cmd = "export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get -q -y install debootstrap nfs-common nfs-kernel-server build-essential host"
  command.run_distant(cmd)

  log("Adding some NFS exports on the server")
  exports = <<EOF
#{NFS_EXPORT_DIR} *(ro,async,no_root_squash,no_subtree_check)
#{NFS_EXPORT_DIR}/tmp *(rw,sync,no_root_squash,no_subtree_check)
#{NFS_EXPORT_DIR}/var *(rw,sync,no_root_squash,no_subtree_check)
#{NFS_EXPORT_DIR}/root *(rw,sync,no_root_squash,no_subtree_check)
EOF
  cmd = "echo '#{exports}' > /etc/exports"
  command.run_distant(cmd)

  log("Creating the NFSROOT")
  if (config.http_proxy != "") then
    cmd = "export http_proxy=#{config.http_proxy} && "
  else
    cmd = ""
  end
  cmd += "mkdir -p #{NFS_EXPORT_DIR} && mkdir #{NFS_EXPORT_DIR}/config && /usr/sbin/debootstrap lenny #{NFS_EXPORT_DIR} http://ftp.fr.debian.org/debian"
  command.run_distant(cmd)

  log("Installing some packages")
  cmd = "chroot #{NFS_EXPORT_DIR} mount -t proc none /proc"
  command.run_distant(cmd)
  if (config.http_proxy != "") then
    cmd = "export http_proxy=#{config.http_proxy} && "
  else
    cmd = ""
  end
  cmd += "chroot #{NFS_EXPORT_DIR} apt-get -q -y --force-yes install #{DEBIAN_PACKAGES}"
  command.run_distant(cmd)
  cmd = "chroot #{NFS_EXPORT_DIR} umount /proc"
  command.run_distant(cmd)

  log("Setting up the Linuxrc")
  linuxrc = <<EOF
#!/bin/bash
/bin/mount -nvt tmpfs none /dev
/bin/mknod -m 622 /dev/console c 5 1
exec /sbin/init 2
EOF
  cmd = "echo '#{linuxrc}' > #{NFS_EXPORT_DIR}/linuxrc && chmod +x #{NFS_EXPORT_DIR}/linuxrc"
  command.run_distant(cmd)

  log("Setting up the authorized keys")
  cmd = "cp -r /root/.ssh #{NFS_EXPORT_DIR}/root"
  command.run_distant(cmd)

  log("Downloading Kernel and Kerrighed files")
  cmd = "cd #{NFS_EXPORT_DIR}/usr/src && wget --no-check-certificate -q #{KERRIGHED_URL} && wget -q --no-check-certificate #{KERNEL_URL}"
  command.run_distant(cmd)

  log("Uncompressing Kerrighed")
  cmd = "cd #{NFS_EXPORT_DIR}/usr/src && tar xzf kerrighed-3.0.0.tar.gz"
  command.run_distant(cmd)

  log("Sending config file")
  command.send(config.config_file, "#{NFS_EXPORT_DIR}/tmp/krg-config")

  log("Compiling Kerrighed")
  script = <<EOF
#!/bin/bash
cd /usr/src/kerrighed-3.0.0
./configure --sysconfdir=/etc --with-kernel-config-file=/tmp/krg-config
make -j #{config.cpu_nb}
make install
ln -sf /etc/network/if-up.d/mountnfs /etc/rcS.d/S35mountnfs
update-rc.d kerrighed-host defaults 99
EOF
  cmd = "echo '#{script}' > #{NFS_EXPORT_DIR}/build_krg.sh"
  command.run_distant(cmd)
  cmd = "chmod +x #{NFS_EXPORT_DIR}/build_krg.sh"
  command.run_distant(cmd)
  cmd = "chroot #{NFS_EXPORT_DIR} /build_krg.sh"
  command.run_distant(cmd)
end

log("Setting Kerrighed configuration")
msg = <<EOF
BOOT_ID=1
ISOLATE_UTS=true
ISOLATE_MNT=true
ISOLATE_NET=false
ISOLATE_USER=false
CLUSTER_INIT_HELPER=/usr/local/sbin/krginit_helper
NET_DEVICES=#{config.eth_device}
EOF
cmd = "echo '#{msg}' > #{NFS_EXPORT_DIR}/etc/default/kerrighed-host"
command.run_distant(cmd)
msg = <<EOF
ADD_OPTS=\\\"--total #{config.slaves.length}\\\"
LEGACY_SCHED=true
EOF
cmd = "echo '#{msg}' > #{NFS_EXPORT_DIR}/etc/default/kerrighed"
command.run_distant(cmd)

log("Getting the Kerrighed kernel")
command.get("#{NFS_EXPORT_DIR}/boot/vmlinuz-2.6.30-krg3.0.0", "krg-kernel")

log("Setting up the fstab")
fstab  = <<EOF
none        /proc    proc   defaults      0 0
none        /sys     sysfs  defaults      0 0
#{master_ip}:#{NFS_EXPORT_DIR}/var  /var  nfs rw,hard,nolock 0 0
#{master_ip}:#{NFS_EXPORT_DIR}/tmp  /tmp  nfs rw,hard,nolock 0 0
#{master_ip}:#{NFS_EXPORT_DIR}/root /root nfs rw,hard,nolock 0 0
none        /var/run tmpfs  defaults      0 0
configfs    /config  configfs defaults    0 0
EOF
cmd = "echo '#{fstab}' > #{NFS_EXPORT_DIR}/etc/fstab"
command.run_distant(cmd)

log("Refreshing the NFS exports")
cmd = "/etc/init.d/nfs-kernel-server restart"
command.run_distant(cmd)

log("Restarting the Kerrighed nodes")
singularities = String.new
node_list = String.new
config.slaves.each_index { |i|
  singularities += "#{config.slaves[i]},#{i + 1}\n"
  node_list += "-m #{config.slaves[i]} "
}
cmd = "\(echo '#{singularities}' > tmp_pxe_profile_singularities\)"
command.run_local(cmd)
pxe = <<EOF
PROMPT 1
SERIAL 0 #{config.console_speed}
DEFAULT bootlabel
DISPLAY messages
TIMEOUT 1
label bootlabel
KERNEL kernels/pxe-#{`id -nu`.chomp}--krg-kernel
APPEND console=tty0 console=ttyS0,#{config.console_speed}n8 session_id=#{config.kerrighed_session} node_id=NODE_SINGULARITY autonodeid=0 rw ip=dhcp nfsroot=#{master_ip}:#{NFS_EXPORT_DIR} init=/linuxrc
EOF
cmd = "\(echo '#{pxe}' > tmp_pxe_profile\)"
command.run_local(cmd)
cmd = "#{KAREBOOT_CMD} -w tmp_pxe_profile -r set_pxe --no-wait -l hard #{node_list} -x krg-kernel --set-pxe-pattern tmp_pxe_profile_singularities"
command.run_local(cmd)

log("Waiting everybody")
sleep(30)
timeout = 200
start = Time.now.to_i
finished = false
while (((Time.now.to_i - start) < timeout) && (not finished))
  finished = true
  config.slaves.each { |current|
    if not Ping.pingecho(current, 1, 2222) then
      finished = false
      break
    end
  }
end
if finished then
  puts "All the nodes have correctly rebooted"
else
  puts "Some nodes do not have rebooted"
end
