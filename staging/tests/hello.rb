#!/usr/bin/ruby -w

# ruby libraries used by scripts
require 'syslog'
require 'socket' 
require 'optparse'
require 'ostruct'
require 'thread'
require 'pty'

# uncomment for error test
#require 'Qt4'

# check ruby version
if (RUBY_VERSION.split('.').map { |i| i.to_i } <=> [ 1, 8, 6 ]) < 0
  puts "You are running Ruby #{RUBY_VERSION}.\nThat is an ancient version of ruby.\nPlease upgrade to 1.8.6 or newer."
  exit 1
end

puts "Hello world from Ruby!"

