maintainer       "Pierre Riteau"
maintainer_email "Pierre.Riteau@irisa.fr"
license          "Apache 2.0"
description      "Installs/Configures Nimbus"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.rdoc'))
version          "0.1"

%w{ java_sun xen }.each do |cb|
  depends cb
end
