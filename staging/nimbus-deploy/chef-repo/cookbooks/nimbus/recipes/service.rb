#
# Cookbook Name:: nimbus
# Recipe:: service
#
# Copyright (c) 2010-2011 Pierre Riteau, University of Rennes 1
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

execute "aptitude update" do
  command "aptitude update"
end

execute "Cleanup /nimbus" do
  command <<-EOH
  if [ -f #{node[:nimbus][:service][:location]}/bin/nimbusctl ]; then #{node[:nimbus][:service][:location]}/bin/nimbusctl stop; fi
  rm -rf /tmp/nimbus_install
  rm -rf #{node[:nimbus][:service][:location]}
  EOH
end

group node[:nimbus][:service][:group] do
end

user node[:nimbus][:service][:user] do
  gid node[:nimbus][:service][:group]
  home node[:nimbus][:service][:location]
  shell "/bin/bash"
end

directory "/tmp/nimbus_install" do
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  mode 0755
end

link node[:nimbus][:service][:location] do
  to "/tmp/nimbus_install"
end

include_recipe "java_sun"

case node[:platform]
when "debian"
  %w{ ant libssl-dev python-dev python-simplejson sqlite3 sun-java6-jdk uuid-runtime }.each do |pkg|
    package pkg
  end
end

remote_file "#{Chef::Config[:file_cache_path]}/#{node[:nimbus][:service][:src_name]}" do
  checksum node[:nimbus][:service][:src_checksum]
  source node[:nimbus][:service][:src_mirror]
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  mode 0755
end

if node[:nimbus][:source]
  remote_file "#{Chef::Config[:file_cache_path]}/ws-core-4.0.8-bin.tar.gz" do
    checksum "a40d7591d2a80dc74274c02ba3d14eff3c5390efff75428e24e475448eab4e84"
    source "http://public.rennes.grid5000.fr/~priteau/chef/ws-core-4.0.8-bin.tar.gz"
    owner node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    mode 0755
  end

  remote_file "#{Chef::Config[:file_cache_path]}/cumulus-deps.tar.gz" do
    checksum "a5e6b884c5331d627614c44774b4e1261bc521828d71fbe5628344ff10581f03"
    source "http://public.rennes.grid5000.fr/~priteau/chef/cumulus-deps.tar.gz"
    owner node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    mode 0755
  end

  execute "Clean up Nimbus sources" do
    cwd "/tmp"
    user node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    command <<-EOH
    rm -rf nimbus
    cp -R #{node[:nimbus][:source]} nimbus
    EOH
  end

  execute "Install Nimbus #{node[:nimbus][:service][:src_version]}" do
    cwd "/tmp/nimbus"
    user node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    environment('HOME' => node[:nimbus][:service][:location])
    command <<-EOH
    mkdir tmp
    cp #{Chef::Config[:file_cache_path]}/ws-core-4.0.8-bin.tar.gz tmp/
    cp #{Chef::Config[:file_cache_path]}/cumulus-deps.tar.gz cumulus/deps
    tar -C cumulus -zxvf cumulus/deps/cumulus-deps.tar.gz
    yes '' | ./install #{node[:nimbus][:service][:location]}
    sed -i -e 's/scp/lantorrent/' #{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/other/authz-callout-ACTIVE.xml
    EOH
  end
else
  execute "Install Nimbus #{node[:nimbus][:service][:src_version]}" do
    cwd "/tmp"
    user node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    environment('HOME' => node[:nimbus][:service][:location])
    command <<-EOH
    rm -rf nimbus-iaas-#{node[:nimbus][:service][:src_version]}-src
    tar -xzf #{Chef::Config[:file_cache_path]}/#{node[:nimbus][:service][:src_name]}
    cd nimbus-iaas-#{node[:nimbus][:service][:src_version]}-src
    yes '' | ./install #{node[:nimbus][:service][:location]}
    sed -i -e 's/scp/lantorrent/' #{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/other/authz-callout-ACTIVE.xml
    EOH
  end
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/ssh.conf" do
  source "ssh.conf"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
    :control_ssh_user => node[:nimbus][:controls][:user],
    :service_ssh_user => node[:nimbus][:service][:user],
    :service_node => node[:fqdn]
  )
end

execute "Remove existing network pools" do
  user "nimbus"
  command <<-EOH
  rm #{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/network-pools/*
  EOH
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/network-pools/public" do
  source "network"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
    :cloudname => node[:nimbus][:service][:cloudname],
    :dns => node[:nimbus][:service][:dns],
    :ip_addresses => node[:nimbus][:service][:ip_addresses],
    :gateway => node[:nimbus][:service][:gateway],
    :broadcast => node[:nimbus][:service][:broadcast],
    :netmask => node[:nimbus][:service][:netmask]
  )
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus/elastic/elastic.conf" do
  source "elastic.conf"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
  )
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus/elastic/other/other-elastic.conf" do
  source "other-elastic.conf"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
    :hypervisor => node[:nimbus][:service][:hypervisor]
  )
end

execute "Disable fake mode" do
  user node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  command "sed -i -e 's/fake.mode=true/fake.mode=false/' #{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/other/common.conf"
  only_if { system "grep 'fake.mode=true' #{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/other/common.conf > /dev/null" }
end

directory "#{node[:nimbus][:service][:location]}/.ssh" do
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  mode 0700
end

execute "Configure SSH" do
  user node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  environment('HOME' => node[:nimbus][:service][:location])
  command <<-EOH
  echo -n '#{node[:nimbus][:service][:ssh_key]}' > #{node[:nimbus][:service][:location]}/.ssh/id_rsa
  echo -n '#{node[:nimbus][:service][:ssh_public_key]}' > #{node[:nimbus][:service][:location]}/.ssh/id_rsa.pub
  echo -n '#{node[:nimbus][:service][:ssh_public_key]}' > #{node[:nimbus][:service][:location]}/.ssh/authorized_keys
  chmod 600 #{node[:nimbus][:service][:location]}/.ssh/*
  EOH
end

%w{ config }.each do |ssh_file|
  remote_file "#{node[:nimbus][:service][:location]}/.ssh/#{ssh_file}" do
    source ssh_file
    owner node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    mode 0600
  end
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus-context-broker/jndi-config.xml" do
  source "jndi-config.xml"
  mode 0600
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
    :service_location => node[:nimbus][:service][:location]
  )
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/metadata.conf" do
  source "metadata.conf"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
    :metadata_server => node[:ipaddress]
  )
end

execute "Increase number of parallel SSH sessions" do
  command <<-EOH
  echo "MaxStartups 1000" >> /etc/ssh/sshd_config
  /etc/init.d/ssh restart
  EOH
  not_if "grep 'MaxStartups 1000' /etc/ssh/sshd_config > /dev/null"
end

execute "Enable container debug" do
  user node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  command <<-EOH
  cat >> #{node[:nimbus][:service][:location]}/services/container-log4j.properties <<EOF
log4j.category.org.globus.workspace=DEBUG
log4j.category.org.nimbustools=DEBUG
EOF
  EOH
  not_if "grep 'log4j.category.org.globus.workspace=DEBUG' #{node[:nimbus][:service][:location]}/services/container-log4j.properties > /dev/null"
end

template "#{node[:nimbus][:service][:location]}/nimbus-setup.conf" do
  source "nimbus-setup.conf"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
    :ca_name => node[:nimbus][:ca_name],
    :service_node => node[:fqdn]
  )
end

template "#{node[:nimbus][:service][:location]}/services/etc/nimbus/workspace-service/other/common.conf" do
  source "common.conf"
  mode 0644
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  variables(
  )
end

execute "Install certificate information" do
  user node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  environment('HOME' => node[:nimbus][:service][:location])
  command <<-EOH
  find #{node[:nimbus][:service][:location]}/var/ca -type f -exec rm {} \\;
  rm #{node[:nimbus][:service][:location]}/var/hostcert.pem #{node[:nimbus][:service][:location]}/var/hostkey.pem #{node[:nimbus][:service][:location]}/var/keystore.jks
  echo -n '#{node[:nimbus][:ca_cert]}' > #{node[:nimbus][:service][:location]}/var/ca/ca-certs/#{node[:nimbus][:ca_name]}.pem
  echo -n '#{node[:nimbus][:ca_key]}' > #{node[:nimbus][:service][:location]}/var/ca/ca-certs/private-key-#{node[:nimbus][:ca_name]}.pem
  CERT_HASH=`openssl x509 -hash -noout < #{node[:nimbus][:service][:location]}/var/ca/ca-certs/#{node[:nimbus][:ca_name]}.pem`
  echo $CERT_HASH
  echo -n '#{node[:nimbus][:ca_cert]}' > #{node[:nimbus][:service][:location]}/var/ca/trusted-certs/$CERT_HASH.0
  cat > #{node[:nimbus][:service][:location]}/var/ca/trusted-certs/$CERT_HASH.signing_policy <<EOF
access_id_CA X509 '/C=US/ST=Several/L=Locality/O=Example/OU=Operations/CN=CA'
pos_rights globus CA:sign
cond_subjects globus '"*"'
EOF
  yes '' | #{node[:nimbus][:service][:location]}/bin/nimbus-configure
  #{node[:nimbus][:service][:location]}/bin/nimbus-new-user -i 620c4a30-90ff-11df-b85b-002332c9497e -s "/C=US/ST=Several/L=Locality/O=Example/OU=Operations/CN=Pierre.Riteau@irisa.fr" -a vIKOIQa5tduXorYRxctyh -p TOZcDmPNFYPii8UTcGXrGiRaW4RNfzm2TcTvqZaNCz Pierre.Riteau@irisa.fr
  EOH
end

service "workspace_service" do
  start_command "su #{node[:nimbus][:service][:user]} sh -c '#{node[:nimbus][:service][:location]}/bin/nimbusctl start'"
  stop_command "su #{node[:nimbus][:service][:user]} sh -c '#{node[:nimbus][:service][:location]}/bin/nimbusctl stop'"
  status_command "su #{node[:nimbus][:service][:user]} sh -c '#{node[:nimbus][:service][:location]}/bin/nimbusctl status'"
  restart_command "su #{node[:nimbus][:service][:user]} sh -c '#{node[:nimbus][:service][:location]}/bin/nimbusctl restart'"
  supports [ :start, :stop, :status, :restart ]
  action [ :restart ]
end

node[:nimbus][:service][:vmm_nodes].each do |vmm|
  execute "Add VMM nodes" do
    cwd node[:nimbus][:service][:location]
    user node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    environment('HOME' => node[:nimbus][:service][:location])
    command <<-EOH
    #{node[:nimbus][:service][:location]}/bin/nimbus-nodes -a #{vmm[:hostname]} -m #{[ (vmm[:ncpus] / 2) * node[:nimbus][:memory_request], vmm[:memory_size].round - 1024 ].min}
    EOH
  end
end
