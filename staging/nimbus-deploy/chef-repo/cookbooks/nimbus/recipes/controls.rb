#
# Cookbook Name:: nimbus
# Recipe:: controls
#
# Copyright (c) 2010-2011 Pierre Riteau, University of Rennes 1
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

execute "aptitude update" do
  command "aptitude update"
end

include_recipe "xen" if node[:nimbus][:controls][:hypervisor] == "xen"

case node[:platform]
when "debian"
  %w{ bridge-utils ebtables isc-dhcp-server python-setuptools python-virtualenv qemu-kvm xinetd }.each do |pkg|
    package pkg
  end
end

execute "Install libvirt and KVM from backports" do
  command "apt-get -t squeeze-backports -y install libvirt-bin python-libvirt"
end

group node[:nimbus][:controls][:group] do
end

user node[:nimbus][:controls][:user] do
  gid node[:nimbus][:controls][:group]
  home node[:nimbus][:controls][:location]
  shell "/bin/bash"
end

group "libvirt" do
  members [ 'nimbus' ]
end

template "/etc/libvirt/qemu.conf" do
  source "qemu.conf"
  owner "root"
  group "root"
  mode 0644
  variables(
  )
end

execute "Restart libvirt" do
  command "/etc/init.d/libvirt-bin restart"
end

execute "Clean up /opt/nimbus" do
  command <<-EOH
  rm -rf /tmp/nimbus_install
  rm -rf #{node[:nimbus][:service][:location]}
  EOH
end

directory "/tmp/nimbus_install" do
  owner "nimbus"
  group "nimbus"
  mode 0755
end

link node[:nimbus][:controls][:location] do
  to "/tmp/nimbus_install"
end

directory "#{node[:nimbus][:controls][:location]}/.ssh" do
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0700
end

execute "Configure SSH" do
  user node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  environment('HOME' => node[:nimbus][:controls][:location])
  command <<-EOH
  echo -n '#{node[:nimbus][:controls][:ssh_key]}' > #{node[:nimbus][:controls][:location]}/.ssh/id_rsa
  echo -n '#{node[:nimbus][:controls][:ssh_public_key]}' > #{node[:nimbus][:controls][:location]}/.ssh/id_rsa.pub
  echo -n '#{node[:nimbus][:controls][:ssh_public_key]}' > #{node[:nimbus][:controls][:location]}/.ssh/authorized_keys
  chmod 600 #{node[:nimbus][:controls][:location]}/.ssh/*
  EOH
end

%w{ config }.each do |ssh_file|
  remote_file "#{node[:nimbus][:controls][:location]}/.ssh/#{ssh_file}" do
    source ssh_file
    owner node[:nimbus][:controls][:user]
    group node[:nimbus][:controls][:group]
    mode 0600
  end
end

remote_file "#{Chef::Config[:file_cache_path]}/#{node[:nimbus][:controls][:src_name]}" do
  checksum node[:nimbus][:controls][:src_checksum]
  source node[:nimbus][:controls][:src_mirror]
  mode 0755
end

remote_file "#{Chef::Config[:file_cache_path]}/simplejson-2.1.6.tar.gz" do
  checksum "b657d4c83a2ffd9c3bdb511eabd986900182870421dceb6b7b117c5407523927"
  source "http://public.rennes.grid5000.fr/~priteau/chef/simplejson-2.1.6.tar.gz"
  owner node[:nimbus][:service][:user]
  group node[:nimbus][:service][:group]
  mode 0755
end

if node[:nimbus][:source]
  execute "Clean up Nimbus source code directory" do
    cwd "/tmp"
    user node[:nimbus][:controls][:user]
    group node[:nimbus][:controls][:group]
    command <<-EOH
    rm -rf nimbus
    cp -R #{node[:nimbus][:source]} nimbus
    EOH
  end

  bash "Install Nimbus control agent" do
    cwd "/tmp/nimbus"
    user node[:nimbus][:controls][:user]
    group node[:nimbus][:controls][:group]
    code <<-EOH
    rm -rf #{node[:nimbus][:controls][:location]}/*
    cp -R control/* #{node[:nimbus][:controls][:location]}
    EOH
  end
else
  execute "Install Nimbus control agent" do
    cwd "/tmp"
    user node[:nimbus][:controls][:user]
    group node[:nimbus][:controls][:group]
    command <<-EOH
    rm -rf #{node[:nimbus][:controls][:location]}/*
    tar -xzf #{Chef::Config[:file_cache_path]}/#{node[:nimbus][:controls][:src_name]}
    cp -R nimbus-iaas-controls-#{node[:nimbus][:controls][:src_version]}/workspace-control/* #{node[:nimbus][:controls][:location]}
    EOH
  end
end

execute "Extract simplejson" do
  cwd "/tmp"
  user node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  command <<-EOH
  rm -rf simplejson-2.1.6
  tar -xzf #{Chef::Config[:file_cache_path]}/simplejson-2.1.6.tar.gz
  EOH
end

execute "Configure lantorrent" do
  if node[:nimbus][:source]
    cwd "/tmp/nimbus/lantorrent"
  else
    cwd "/tmp/nimbus-iaas-controls-#{node[:nimbus][:controls][:src_version]}/workspace-control/lantorrent"
  end
  user node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  environment('HOME' => node[:nimbus][:controls][:location])
  command <<-EOH
  mkdir -p ~/.lantorrent/etc
  virtualenv #{node[:nimbus][:controls][:location]}/lantorrent
  sh -c 'cd /tmp/simplejson-2.1.6 && #{node[:nimbus][:controls][:location]}/lantorrent/bin/python setup.py install'
  #{node[:nimbus][:controls][:location]}/lantorrent/bin/python setup-vmm.py install
  EOH
end

template "/etc/xinetd.d/lantorrent" do
  source "lantorrent"
  mode 0644
  variables(
    :nimbus_location => node[:nimbus][:controls][:location],
    :nimbus_user => node[:nimbus][:controls][:user]
  )
end

execute "Restart xinetd" do
  command <<-EOH
  while pgrep xinetd; do
    /etc/init.d/xinetd stop
  done
  ps aux | grep xinet
  ls -l /var/run
  if /etc/init.d/xinetd start | grep failed; then
    ps aux | grep xinet
    ls -l /var/run
    exit 1
  fi
  EOH
end

execute "sudoers" do
  command "(cat >> /etc/sudoers <<EOF
nimbus ALL=(root) NOPASSWD: #{node[:nimbus][:controls][:location]}/libexec/workspace-control/mount-alter.sh
nimbus ALL=(root) NOPASSWD: #{node[:nimbus][:controls][:location]}/libexec/workspace-control/dhcp-config.sh
nimbus ALL=(root) NOPASSWD: #{node[:nimbus][:controls][:location]}/libexec/workspace-control/xen-ebtables-config.sh
nimbus ALL=(root) NOPASSWD: #{node[:nimbus][:controls][:location]}/libexec/workspace-control/kvm-ebtables-config.sh
EOF
)"
end

template "#{node[:nimbus][:controls][:location]}/etc/workspace-control/kernels.conf" do
  source "kernels.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
    :hypervisor => node[:nimbus][:controls][:hypervisor]
  )
end

template "#{node[:nimbus][:controls][:location]}/etc/workspace-control/networks.conf" do
  source "networks.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
    :hypervisor => node[:nimbus][:controls][:hypervisor],
    :bridge => "#{node[:nimbus][:controls][:hypervisor] == 'xen' ? node[:network][:default_interface]: "br0"}",
    :interface => "#{node[:nimbus][:controls][:hypervisor] == 'xen' ? "p#{node[:network][:default_interface]}": node[:network][:default_interface]}"
  )
end

template "#{node[:nimbus][:controls][:location]}/etc/workspace-control/propagation.conf" do
  source "propagation.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
  )
end

template "#{node[:nimbus][:controls][:location]}/etc/workspace-control/xen.conf" do
  source "xen.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
  )
end

template "/etc/dhcp/dhcpd.conf" do
  source "dhcpd.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
    :default_network => node[:network][:interfaces][node[:network][:default_interface]][:addresses].select { |addr, details| details[:family] == "inet" }[0][0],
    :default_netmask => "255.255.255.255",
    :network => node[:nimbus][:controls][:network],
    :netmask => node[:nimbus][:controls][:netmask]
  )
end

execute "dhcp-config.sh" do
  command "(cd #{node[:nimbus][:controls][:location]};
    sed -i -e 's/DHCPD_CONF=\"\\/etc\\/dhcpd.conf\"/DHCPD_CONF=\"\\/etc\\/dhcp\\/dhcpd.conf\"/' libexec/workspace-control/dhcp-config.sh;
    sed -i -e 's/DHCPD_STOP=\"\\/etc\\/init.d\\/dhcpd stop\"/DHCPD_STOP=\"\\/etc\\/init.d\\/isc-dhcp-server stop\"/' libexec/workspace-control/dhcp-config.sh;
    sed -i -e 's/DHCPD_START=\"\\/etc\\/init.d\\/dhcpd start\"/DHCPD_START=\"\\/etc\\/init.d\\/isc-dhcp-server start\"/' libexec/workspace-control/dhcp-config.sh;
  )"
end

template "#{node[:nimbus][:controls][:location]}/etc/workspace-control/libvirt.conf" do
  source "libvirt.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
    :hypervisor => "#{node[:nimbus][:controls][:hypervisor] == "xen"? "xen3": "kvm0"}"
  )
end

template "#{node[:nimbus][:controls][:location]}/etc/workspace-control/xen.conf" do
  source "xen.conf"
  owner node[:nimbus][:controls][:user]
  group node[:nimbus][:controls][:group]
  mode 0644
  variables(
  )
end

if node[:nimbus][:controls][:hypervisor] == "xen"
  execute "Install kernels" do
    command <<-EOH
    cp /boot/vmlinuz-2.6.32-5-xen-amd64 #{node[:nimbus][:controls][:location]}/var/workspace-control/kernels/
    cp /boot/initrd.img-2.6.32-5-xen-amd64 #{node[:nimbus][:controls][:location]}/var/workspace-control/kernels/vmlinuz-2.6.32-5-xen-amd64-initrd
    chown #{node[:nimbus][:controls][:user]}:#{node[:nimbus][:controls][:group]} #{node[:nimbus][:controls][:location]}/var/workspace-control/kernels/*
    EOH
  end
end

execute "setup-bridge" do
  command <<-EOH
  sed -i -e 's/^.*#{node[:network][:default_interface]}.*$//' /etc/network/interfaces;
  cat >> /etc/network/interfaces <<EOF
auto br0
iface br0 inet dhcp
  bridge_ports #{node[:network][:default_interface]}
  bridge_stp on
  bridge_maxwait 0
  bridge_fd 0
EOF
  /etc/init.d/networking restart
  EOH
  not_if "grep br0 /etc/network/interfaces > /dev/null 2>&1" || node[:nimbus][:controls][:hypervisor] == "xen"
end

template "/etc/default/isc-dhcp-server" do
  source "isc-dhcp-server"
  owner "root"
  group "root"
  mode 0644
  case node[:nimbus][:controls][:hypervisor]
  when "xen"
  variables(
    :interface => "#{node[:network][:default_interface]}"
  )
  when "kvm"
  variables(
    :interface => "br0"
  )
  end
end

execute "run-dhcpd" do
  command "/etc/init.d/isc-dhcp-server start"
end

execute "Increase number of parallel SSH sessions" do
  command <<-EOH
  echo "MaxStartups 1000" >> /etc/ssh/sshd_config
  /etc/init.d/ssh restart
  EOH
  not_if "grep 'MaxStartups 1000' /etc/ssh/sshd_config > /dev/null"
end
