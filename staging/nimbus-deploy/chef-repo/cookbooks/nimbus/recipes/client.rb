#
# Cookbook Name:: nimbus
# Recipe:: client
#
# Copyright (c) 2010-2011 Pierre Riteau, University of Rennes 1
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

execute "aptitude update" do
  command "aptitude update"
end

include_recipe "java_sun"

group node[:nimbus][:client][:group] do
end

user node[:nimbus][:client][:user] do
  gid node[:nimbus][:client][:group]
  home node[:nimbus][:service][:location]
  shell "/bin/bash"
end

directory node[:nimbus][:service][:location] do
  owner node[:nimbus][:client][:user]
  group node[:nimbus][:client][:group]
  mode 0755
end

if node[:nimbus][:source]
  package "ant"

  remote_file "#{Chef::Config[:file_cache_path]}/ws-core-4.0.8-bin.tar.gz" do
    checksum "a40d7591d2a80dc74274c02ba3d14eff3c5390efff75428e24e475448eab4e84"
    source "http://public.rennes.grid5000.fr/~priteau/chef/ws-core-4.0.8-bin.tar.gz"
    owner node[:nimbus][:service][:user]
    group node[:nimbus][:service][:group]
    mode 0755
  end

  execute "Clean up Nimbus source code directory" do
    cwd "/tmp"
    user node[:nimbus][:client][:user]
    group node[:nimbus][:client][:group]
    command <<-EOH
    rm -rf nimbus
    cp -R #{node[:nimbus][:source]} nimbus
    EOH
  end

  execute "Install Nimbus cloud client" do
    user node[:nimbus][:client][:user]
    group node[:nimbus][:client][:group]
    cwd "/tmp/nimbus"
    command <<-EOH
    set -e
    cp #{Chef::Config[:file_cache_path]}/ws-core-4.0.8-bin.tar.gz cloud-client/
    cd cloud-client
    bash builder/dist.sh
    rm -rf #{node[:nimbus][:client][:location]}/nimbus-cloud-client-#{node[:nimbus][:client][:src_version]}
    mv nimbus-cloud-client-#{node[:nimbus][:client][:src_version]} #{node[:nimbus][:client][:location]}/
    EOH
  end
else
  remote_file "#{Chef::Config[:file_cache_path]}/#{node[:nimbus][:client][:src_name]}" do
    checksum node[:nimbus][:client][:src_checksum]
    source node[:nimbus][:client][:src_mirror]
    mode 0755
  end

  execute "Install Nimbus cloud client" do
    user node[:nimbus][:client][:user]
    group node[:nimbus][:client][:group]
    cwd node[:nimbus][:client][:location]
    command <<-EOH
    tar -xzf #{Chef::Config[:file_cache_path]}/#{node[:nimbus][:client][:src_name]}
    EOH
  end
end

execute "Install root CA" do
  cwd node[:nimbus][:client][:location]
  command <<-EOH
    CERT_HASH=`echo -n '#{node[:nimbus][:ca_cert]}' | openssl x509 -hash -noout`
    echo -n '#{node[:nimbus][:ca_cert]}' > #{node[:nimbus][:client][:location]}/nimbus-cloud-client-#{node[:nimbus][:client][:src_version]}/lib/certs/$CERT_HASH.0
    chmod 644 #{node[:nimbus][:client][:location]}/nimbus-cloud-client-#{node[:nimbus][:client][:src_version]}/lib/certs/$CERT_HASH.0
    cat > #{node[:nimbus][:client][:location]}/nimbus-cloud-client-#{node[:nimbus][:client][:src_version]}/lib/certs/$CERT_HASH.signing_policy <<EOF
access_id_CA X509 '/C=US/ST=Several/L=Locality/O=Example/OU=Operations/CN=CA'
pos_rights globus CA:sign
cond_subjects globus '"*"'
EOF
  EOH
end

execute "Generate SSH credentials" do
  user node[:nimbus][:client][:user]
  group node[:nimbus][:client][:group]
  environment('HOME' => node[:nimbus][:client][:location])
  command <<-EOH
  umask 077
  mkdir -p #{node[:nimbus][:client][:location]}/.ssh
  echo -n '#{node[:nimbus][:client][:ssh_key]}' > #{node[:nimbus][:client][:location]}/.ssh/id_rsa
  echo -n '#{node[:nimbus][:client][:ssh_public_key]}' > #{node[:nimbus][:client][:location]}/.ssh/id_rsa.pub
  touch ~/.ssh/known_hosts
  egrep 'StrictHostKeyChecking[[:space:]]+no' ~/.ssh/config > /dev/null || echo 'StrictHostKeyChecking no' >> ~/.ssh/config
  EOH
end

execute "Allow users to connect to the nimbus account from the frontend" do
  command <<-EOH
  cat /root/.ssh/authorized_keys >> #{node[:nimbus][:client][:location]}/.ssh/authorized_keys
  chown #{node[:nimbus][:client][:user]}:#{node[:nimbus][:client][:group]} #{node[:nimbus][:client][:location]}/.ssh/authorized_keys
  EOH
end

execute "Delete old nimbus configuration" do
  command <<-EOH
  rm -rf #{node[:nimbus][:client][:location]}/nimbus-cloud-client-#{node[:nimbus][:client][:src_version]}/conf/*
  EOH
end

template "#{node[:nimbus][:client][:location]}/nimbus-cloud-client-#{node[:nimbus][:client][:src_version]}/conf/cloud.properties" do
  source "cloud.properties"
  mode 0644
  owner node[:nimbus][:client][:user]
  group node[:nimbus][:client][:group]
  variables(
    :memory_request => node[:nimbus][:memory_request],
    :service_node => node[:nimbus][:client][:service_node],
    :hypervisor => node[:nimbus][:client][:hypervisor]
  )
end

execute "Install user certificate" do
  user node[:nimbus][:client][:user]
  group node[:nimbus][:client][:group]
  environment('HOME' => node[:nimbus][:client][:location])
  cwd node[:nimbus][:client][:location]
  command <<-EOH
  umask 077
  mkdir -p .globus
  echo -n "#{node[:nimbus][:user_cert]}" > .globus/usercert.pem
  echo -n "#{node[:nimbus][:user_key]}" > .globus/userkey.pem
  EOH
end
