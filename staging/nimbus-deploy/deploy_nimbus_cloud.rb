#!/usr/bin/env ruby

begin
  require 'rubygems'
  require 'gosen'
  require 'json'
  require 'logger'
  require 'net/scp'
  require 'net/ssh'
  require 'restfully'
  require 'tempfile'
  require 'tmpdir'
  require 'yaml'
rescue LoadError => e
  warn e
  abort "error: this script depends on a few gems: please run `gem install gosen json net-ssh net-scp restfully' first."
end

if !File.exists?(File.expand_path("~/public/config"))
  Dir.mkdir(File.expand_path("~/public/config"))
end

if !File.directory?(File.expand_path("~/public/config"))
  abort 'This script relies on having access to a ~/public/config directory, but there is already a file with this name'
end

@logger = Logger.new(STDOUT)
@restfully_logger = Logger.new(STDERR)
@restfully_logger.level = Logger::WARN
@grid = Restfully::Session.new(:base_uri => 'https://api.grid5000.fr/2.0/grid5000', :logger => @restfully_logger).root
@jobs = []
@job_status = []
@nodes = Hash.new { |hash, key| hash[key] = Hash.new }
@site = `hostname | awk 'BEGIN { FS = "." }; { print $2 }'`.chomp
@needed_nodes = 3 # service/repository + client + 1 VMM
@username = ENV['USER']

if ARGV[0]
  @job_status = YAML.load_file(ARGV[0])
  @job_status.each do |job|
    @jobs.push(@grid.sites[job['site'].to_sym].jobs[:"#{job['uid']}"].reload)
  end
else
  puts "Usage: deploy_nimbus_cloud.rb jobs.yml"
  puts
  puts <<EOH
The deploy_nimbus_cloud.rb script deploys and configures several Nimbus clouds,
each corresponding to a (running) OAR job. The only argument of the script
is a YAML file describing these jobs.  An example of a file is:

---
- hypervisor: kvm
  uid: 374466
  site: rennes
- hypervisor: kvm
  uid: 320215
  site: nancy

Don't forget the dashes and the spacing, it's required! Create a file with this
content, and customize it with your job details. For now only one cloud per
site is supported (it is easy to fix though, if you need this feature ask me).
You can of course run the script with only one job. "kvm" can be replaced by
"xen" to deploy VMMs based on the Xen hypervisor.

The script will deploy nodes, install and configure Nimbus and its
dependencies, and run the necessary services. At the end, the script prints the
name of the node to use to contact the Nimbus service.

Internally, the script works in the following way. It deploys a Kadeploy image
able to run the Nimbus software software components and their dependencies.
These images are stored in ~priteau/public. Next, it allocates roles to each
node in the deployment and uses Chef to configure them. The Chef scripts are
located in chef-repo. Chef install Nimbus and its dependencies (libvirt, etc.).
At the end of the script, the user can connect to a node with a configured
cloud client in order to interact with Nimbus and deploy VMs.

By default the script will install Nimbus from the official tarball release.
It is possible to install from a directory containing the Nimbus source tree
instead. Change your YAML file to include a source property:

---
- hypervisor: kvm
  uid: 374466
  site: rennes
  source: /home/priteau/nimbus
- hypervisor: kvm
  uid: 320215
  site: nancy
  source: /home/priteau/nimbus

Note that the source tree needs to be stored on each site's NFS server.
EOH
exit 1
end

abort "Not enough nodes found on the grid" if @jobs.empty?

def collect_nodes(path, hash = {})
  nodes = nil
  File.open(File.expand_path(path), 'r') do |f|
    nodes = f.readlines.collect { |l| l.chomp }.uniq
    if hash.has_key?(:number)
      number = hash[:number]
      if nodes.size < number
        abort "error: not enough nodes: only #{nodes.size}, expected #{number}"
      else
        nodes = nodes[0, number]
      end
    end
  end
  return nodes
end

@job_status.reject { |j| j['good_nodes'].nil? || j['good_nodes'].empty? }.each do |job_status|
  @nodes[job_status['site']][job_status['uid']] = job_status['good_nodes']
end

@threads = []
@job_status.select { |j| j['good_nodes'].nil? || j['good_nodes'].empty? }.each do |job_status|
  job = @jobs.select { |j| job_status['uid'] == j['uid'] && job_status['site'] == j.parent['uid'] }.first
  abort "Job #{job['uid']} in #{job.parent['name']} is not running" if job['state'] != 'running'
  Net::SSH.start(job.parent['uid'], ENV['USER'], :timeout => 5) do |ssh|
    if ssh.exec!("g5k-subnets -j #{job['uid']}").nil?
      abort <<EOH
Job #{job['uid']} in #{job.parent['name']} doesn't have any subnet reservation.
You must reserve at least one subnet, for instance with "oarsub -I -l nodes=3+slash_22=1 -t deploy"
EOH
    end
  end

  @threads << Thread.new() do
    site = job.parent
    nodes = job['assigned_nodes']
    begin
      env_dsc = case job_status['hypervisor']
      when "xen"
        "http://public.rennes.grid5000.fr/~priteau/descriptions/squeeze-x64-xen-chef.dsc"
      when "kvm"
        "http://public.rennes.grid5000.fr/~priteau/descriptions/squeeze-x64-nfs-chef.dsc"
      else
        abort "hypervisor #{job_status['hypervisor']} unknown"
      end

      # Bugzilla 983
      if site['uid'] == 'toulouse'
        abort "Toulouse doesn't support virtual IPs, aborting... (see https://www.grid5000.fr/cgi-bin/bugzilla3/show_bug.cgi?id=983)"
      end

      if job_status['hypervisor'] == 'kvm'
        clusters = nodes.map { |n| n.sub(/^([^-]*).*$/, '\1') }.uniq
        clusters.each do |cluster|
          abort "You asked for the KVM hypervisor but #{cluster} doesn't support hardware virtualization" if site.clusters[cluster.to_sym].nodes.first['supported_job_types']['virtual'] == false
        end
      end

      deployment_options = {
          :logger => @logger,
          :max_deploy_runs => 2,
          :min_deployed_nodes => [@needed_nodes, (nodes.size * 0.8).round].max,
          :ssh_public_key => File.read(File.expand_path('~/.ssh/authorized_keys'))
      }
      deployment = Gosen::Deployment.new(site, env_dsc, nodes, deployment_options)
      deployment.join
    rescue Gosen::Error => e
      puts e
      job = @jobs.select { |j| j.parent['uid'] == site['uid'] }.first
    else
      @nodes[site['uid']][job['uid']] = deployment.good_nodes
      job_status = @job_status.select { |j| j["site"] == site['uid'] && j["uid"] == job['uid'] }.first
      job_status["good_nodes"] = deployment.good_nodes
    end
  end
end
@threads.each { |thr| thr.join }

File.open(ARGV[0], 'w') do |f|
  f.puts YAML::dump(@job_status)
end

def short_name(nodename)
  if nodename =~ /(^[a-z]+-[0-9]+)\.[a-z]+\.grid5000\.fr$/
    return $1
  else
    raise StandardError
  end
end

# Generate a root CA
ca_key = Tempfile.new('tmp')
ca_cert = Tempfile.new('tmp')
`openssl genrsa 2048 > #{ca_key.path} 2> /dev/null`
`openssl req -subj "/C=US/ST=Several/L=Locality/O=Example/OU=Operations/CN=CA" -new -x509 -nodes -sha1 -key #{ca_key.path} -out #{ca_cert.path} 2> /dev/null`

# Generate a user certificate request...
user_name = "Pierre.Riteau@irisa.fr"
user_key = Tempfile.new('tmp')
user_request = Tempfile.new('tmp')
user_cert = Tempfile.new('tmp')
`openssl genrsa 2048 > #{user_key.path} 2> /dev/null`
`openssl req -subj "/C=US/ST=Several/L=Locality/O=Example/OU=Operations/CN=#{user_name}" -new -nodes -key #{user_key.path} -out #{user_request.path} 2> /dev/null`
# ... and sign it
`openssl x509 -req -days 365 -in #{user_request.path} -CA #{ca_cert.path} -CAkey #{ca_key.path} -set_serial 01 -out #{user_cert.path} 2> /dev/null`

# Generate an SSH keypair
tmpdir = Dir.mktmpdir
ssh_key_file = File.join(tmpdir, 'id_rsa')
ssh_public_key_file = File.join(tmpdir, 'id_rsa.pub')
`ssh-keygen -f #{ssh_key_file} -N ''`
ssh_key = File.read(ssh_key_file)
ssh_public_key = File.read(ssh_public_key_file)

@config = {}
@clouds = []

@jobs.each do |job|
  site = job.parent['uid']
  nodes = @nodes[site][job['uid']]
  service_node = nodes[0]
  job_status = @job_status.select { |j| j["site"] == site && j["uid"] == job['uid'] }.first

  site_nodes = @grid.sites[site.to_sym].clusters.map { |c| c.nodes.map { |n| n } }.flatten
  vmm_nodes = []
  nodes[2..-1].each do |nodename|
    node = site_nodes.select { |n| n['uid'] == short_name(nodename) }.first
    raise StandardError if node.nil?
    ncpus = node['architecture']['smt_size']
    memory_size = node['main_memory']['ram_size'] / (1024 * 1024)
    vmm_nodes.push({ :hostname => nodename, :ncpus => ncpus, :memory_size => memory_size })
  end
  total_cpus = vmm_nodes.inject(0) { |sum, node| sum + node[:ncpus] }

  dns = network = broadcast = netmask = gateway = ""
  # Get the DNS server address
  Net::SSH.start(site, ENV['USER'], :timeout => 5) do |ssh|
    dns = ssh.exec!("cat /etc/resolv.conf | awk '/^nameserver/ { print $2; exit }'").chomp
  end

  ip_addresses = []
  Net::SSH.start(site, ENV['USER'], :timeout => 5) do |ssh|
    ip_addresses = ssh.exec!("g5k-subnets -j #{job['uid']} -i").lines.map { |l| l.chomp }
    network, broadcast, netmask, gateway = ssh.exec!("g5k-subnets -j #{job['uid']} -a").first.chomp.sub(/\/[0-9]+/, '').split(" ")
  end

  case site
  when 'bordeaux'
    network = '10.128.0.0'
  when 'grenoble'
    network = '10.180.0.0'
  when 'lille'
    network = '10.136.0.0'
  when 'lyon'
    network = '10.140.0.0'
  when 'luxembourg'
    network = '10.172.0.0'
  when 'nancy'
    network = '10.144.0.0'
  when 'orsay'
    network = '10.148.0.0'
  when 'reims'
    network = '10.168.0.0'
  when 'rennes'
    network = '10.156.0.0'
  when 'sophia'
    network = '10.164.0.0'
  when 'toulouse'
    network = '10.160.0.0'
  else
    abort "error: unknown site #{site}"
  end

  @config[service_node] =
  {
    "nimbus" => {
      "ca_cert" => File.read(ca_cert.path),
      "ca_key" => File.read(ca_key.path),
      "ca_name" => "NimbusCA",
      "user_name" => user_name,
      "user_cert" => File.read(user_cert.path),
      "service" => {
        "cloudname" => "#{site}-#{job['uid']}",
        "dns" => dns,
        "ip_addresses" => ip_addresses,
        "vmm_nodes" => vmm_nodes,
        "gateway" => gateway,
        "broadcast" => broadcast,
        "netmask" => netmask,
        "ssh_key" => ssh_key,
        "ssh_public_key" => ssh_public_key,
        "hypervisor" => job_status['hypervisor'],
      }
    },
    "recipes" => [ "nimbus::service" ]
  }

  vmm_nodes.each_with_index do |vmm_node, i|
    @config[vmm_node[:hostname]] =
    {
      "nimbus" => {
        "controls" => {
          "network" => network,
          "netmask" => netmask,
          "hypervisor" => job_status['hypervisor'],
          "ssh_key" => ssh_key,
          "ssh_public_key" => ssh_public_key
        }
      },
      "recipes" => [ "nimbus::controls" ]
    }
  end

  @clouds.push({ :name => "#{site}-#{job['uid']}", :site => site, :uid => job['uid'], :service_node => service_node, :nb_vmms => vmm_nodes.length, :hypervisor => job_status['hypervisor'] })
end

@clouds.each do |cloud|
  nodes = @nodes[cloud[:site]][cloud[:uid]]
  client_node = nodes[1]
  cloud[:client_node] = client_node

  @config[client_node] =
  {
    "nimbus" => {
      "user_cert" => File.read(user_cert.path),
      "user_key" => File.read(user_key.path),
      "ca_cert" => File.read(ca_cert.path),
      "client" => {
        "service_node" => cloud[:service_node],
        "ssh_key" => ssh_key,
        "ssh_public_key" => ssh_public_key,
        "hypervisor" => cloud[:hypervisor]
      },
    },
    "recipes" => [ "nimbus::client" ]
  }
end

@job_status.each do |job_status|
  job_status['good_nodes'].each do |node|
    @config[node]['nimbus']['source'] = job_status['source'] if job_status['source']
  end
end

@job_status = YAML.load_file(ARGV[0])
all_nodes_file = Tempfile.new('tmp')
File.open(all_nodes_file.path, 'w') do |f|
  @config.each do |node, config|
    f.puts node
  end
end

`kaput -F -l root -M #{all_nodes_file.path} #{ssh_key_file} /root/.ssh/id_rsa`
`kaput -F -l root -M #{all_nodes_file.path} #{ssh_public_key_file} /root/.ssh/id_rsa.pub`
`kash -F -l root -M #{all_nodes_file.path} -- 'cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys'`

solo_tarball = Tempfile.new('tmp')
system "tar -C #{File.dirname(File.expand_path(__FILE__))}/chef-repo -zcf #{solo_tarball.path} ./cookbooks"
system "kaput -l root -M #{all_nodes_file.path} #{solo_tarball.path} /tmp/"

@config.each do |node, config|
  File.open(File.expand_path("~/public/config/#{node}"), 'w') do |f|
    f.puts(JSON.pretty_generate(config))
  end
end

@domain = `hostname --fqdn`.chomp.sub(/^[^\.]*\./, '')
`kash -l root -M #{all_nodes_file.path} -- mkdir -p /var/chef/cache`
@logger.info "Broadcasting node configurations"
`kash -l root -M #{all_nodes_file.path} -- wget -q -O /tmp/node.json http://public.#{@domain}/~#{@username}/config/\\\`hostname --fqdn\\\``
solo_basename = `basename #{solo_tarball.path}`.chomp
@logger.info "Configuring and installing Nimbus (please wait)"
`kash -l root -M #{all_nodes_file.path} -- 'chef-solo -l debug -j /tmp/node.json -r /tmp/#{`basename #{solo_tarball.path}`.chomp} | tee /tmp/chef.log'`
@logger.info "Installation finished"

string = @clouds.map do |cloud|
%Q{Nimbus cloud in #{cloud[:name]}:
- service node is #{cloud[:service_node]}
- client node is #{cloud[:client_node]} (connect with "ssh nimbus@#{cloud[:client_node]}")
}
end.join("\n");

puts
puts string
