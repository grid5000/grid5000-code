# Class KadeployWaiter
# 
# Author:: David Margery (mailto:david.margery@inria.fr)
# Copyright:: Copyright (c) 2010 INRIA
# Licence:: Free to use, distribute, change, distribute changed versions for Grid'5000 users

require 'webrick'
require 'json'
require 'Kaenv.rb'

# This class implements a webrick server waiting for notifications as sent by Kadeploy3
# It will find an open port to avoid conflicts with other people running the same script.
# Typical usage would be
#
#      waiter=KadeployWaiter.new
#      deployment = {
#	:nodes => nodes,
#	:environment => envname,
#	:key => key_url,
#	:notifications => [waiter.getUrl]
#      }
#      api[site['links'].detect{|l| l['title'] == 'deployments'}['href']].post(
#         JSON.dump(deployment),
#         :content_type => 'application/json',
#         :accept => 'application/json'    ) do |response|
#	 case response.code
#	 when 201
#	   submitted_deployment = JSON.parse(response.body)
#	   puts "OK, Deployment submitted:"
#	   puts "Waiting until deployment is finished..."
#	   submitted_deployment=waiter.waitEndDeployment(submitted_deployment,600)
# 	 else
#	   puts "Error: #{response.code} - #{JSON.parse response.body}"
#	 end
#      end 

class KadeployWaiter
  class KadeployWaiterServlet < WEBrick::HTTPServlet::AbstractServlet 
    def do_POST(request,response)
      @waiter.body=request.body
      @server.shutdown
      response.status=200
      response['Content-Type'] = 'text/plain'
      response.body= 'Seen Kadeploy !!'
    end

    def initialize(server, waiter)
      super(server)
      @waiter=waiter
    end
  end

  class TarFileServlet < WEBrick::HTTPServlet::FileHandler 
    def do_GET(request,response)
      puts "  KadeployWaiter::INFO Got a #{request.request_method} request for #{request.path} when serving files ending by #{@file}"
      if File.basename(request.path) == @file
	super(request,response)
      end
    end

    def initialize(server, file_to_serve)
      super(server,File.dirname(file_to_serve))
      @file=File.basename(file_to_serve)
    end
  end

  class EnvironmentServlet < WEBrick::HTTPServlet::AbstractServlet 
    def do_GET(request,response)
      response.status=200
      response['Content-Type'] = 'text/plain'
      response.body= @env.to_s
    end

    def initialize(server, env)
      super(server)
      @env=env
    end
  end

  # A log class that can intercept logs that an exception has occured
  # a raise the exception again, because webrick catches all exceptions
  # and therefore timeouts don't time out.
  # very simple and fragile implementation for Timoout::Error
  class RelayTimeoutExceptionLog < WEBrick::Log
    def log(level,data)
       if data =~ /execution expired/
         raise Timeout::Error
       end	
    end
    def method_missing(method, *args)
      nil
    end
  end
  
  # Url the #KadeployWaiter instance can be notified on
  attr_reader :url

  # the underlying webrick server
  attr_reader :s

  # the optional environment published
  attr_reader :env

  # Attribute used to pass the contents of the notification from
  # the #KadeployWaiterServlet to the running #KadeployWaiter
  attr_accessor :body

  # Start a new server. Find an open port for that server between
  # start_port and stop_port
  # Raises Errno::EADDRINUSE if no port could be found
  # Could benefit from randomization of search in case of concurrent
  # walks by different instances.
  def initialize(start_port=0, stop_port=0)
    @env=nil
    @webrick_started=false
    port=start_port
    if stop_port==0 && start_port != 0
      stop_port=start_port+100
    end

    nologs=RelayTimeoutExceptionLog.new
    raise Errno::EINVAL if stop_port < start_port 
    begin
      @s = WEBrick::HTTPServer.new(:Port => port, :Logger => nologs, :AccessLog => nologs)
    rescue Errno::EADDRINUSE => e
      port=port+1
      retry unless port > stop_port
      raise e if port>stop_port
    else
      @url="http://#{Socket.gethostname}:#{@s.config[:Port]}"
    end   
  end
  
  # get the URL the waiter is expecting a notification on
  def getUrl
    return @url+"/kadeploy"
  end

  # wait for notification or timeout
  # *submitted_deployment: the result to return if time out
  # *timout: time to wait for the notification. O is forever
  def waitEndDeployment(submitted_deployment, timeout=0)
    @s.mount "/kadeploy", KadeployWaiterServlet, self
    if timeout != 0
      begin
	status = Timeout::timeout(timeout) {
	  if @webrick_started
	    @server.join
	  else
	    @webrick_started=true
	    @s.start
	  end
	}
      rescue Timeout::Error
	@s.shutdown
	@webrick_started=false
	return submitted_deployment
      end
    else
      if @webrick_started
	@server.join
      else
	@webrick_started=true
	@s.start
      end
    end
    return JSON.parse(@body)
  end

  # use the web server that was started to publish an environment
  # described in a file
  def publish(filename)
    @env=Kaenv.new(filename)

    tarball=@env.tarball.split('|')
    @s.mount "/tarball", TarFileServlet, tarball[0]
    tarball[0]=@url+"/tarball/"+File.basename(tarball[0])
    @env.tarball=tarball.join('|')

    postinstall=@env.postinstall.split('|')
    @s.mount "/postinstall", TarFileServlet, postinstall[0]
    postinstall[0]=@url+"/postinstall/"+File.basename(postinstall[0])
    @env.postinstall=postinstall.join('|')

    @s.mount "/environment", EnvironmentServlet, @env
    @server=Thread.new {
      @webrick_started=true
      @s.start
    }
    return @url+"/environment"
  end
end
