# Class Kaenv
# 
# Author:: David Margery (mailto:david.margery@inria.fr)
# Copyright:: Copyright (c) 2010 INRIA
# Licence:: Free to use, distribute, change, distribute changed versions for Grid'5000 users

require 'restclient'

# A simple class to manipulate Kadeploy3 environments
# This class attempts to abstract the source of the environment 
# (file, url, registred environment), get all information,
# let the user manipulate the attribute and at last save in a format kaenv3 understands
class Kaenv
   # Classical environment attributes
   attr_accessor :name, :description, :author, :visibility, :tarball
   # Classical environment attributes
   attr_accessor :postinstall, :kernel, :initrd, :hypervisor, :hypervisor_params
   # Classical environment attributes
   attr_accessor :fdisktype, :filesystem, :environment_kind, :demolishing_env
   # Classical environment attributes
   attr_accessor :version

   #remember if the environment was read from a file or url
   @from_file_or_url=false
   #remember the name of the environment
   @file_or_url=''

  # internal method
   def save_line(line)
     contents= line.chomp!.split(':',2)
        begin
	  self.send((contents[0].rstrip+"=").to_sym,contents[1].lstrip)
        rescue NoMethodError => e
	   raise e unless contents[0]==nil || contents[0].match("#") 
	end
   end	

   # * description is either the name of the environment as known by kaenv3, or the name of a file with the description, or an url with the description
   # * user user to query when requesting info about description to kaenv  
   def initialize(description,user=nil)
     description_file=$sdtin
     if(File.exists?(description))
        @from_file_or_url=true
        @file_or_url=description
	description_file=File.new(description,'r')
	line=description_file.readline
     elsif description.match('://')
       remote_file= RestClient::Resource.new(description, :timeout => 5)
       remote_file.get() do |response|
	 case response.code
	 when 200
	   @from_file_or_url=true
	   @file_or_url=description
	   description_file=StringIO::new(response.body)
	   line=description_file.readline
	 else
	   raise "Environement #{description} not found remotely"
	 end
       end
     else
       if user 
	 description_file=IO.popen("kaenv3 -p #{description} -u #{user}")
       else
	 description_file=IO.popen("kaenv3 -p #{description}")
       end
       line=description_file.readline
       if line.match('The environment does not exist*') && user==nil
	 description_file.close
	 description_file=IO.popen("kaenv3 -p #{description} -u deploy")
	 line=description_file.readline
	 if line.match('The environment does not exist*')
	   raise "Environement #{description} not found"
	 end
       end
     end
     save_line(line)
     description_file.each do |line|
       save_line(line)
     end
     description_file.close
   end
 
  # show the name that the API could use to manipulate this environment.
  def apiname
     if @from_file_or_url
	@file_or_url
     else
        @name
     end
  end

  # output to a kaenv3 comptible format
  def to_s
     "name : "+@name+"\n"+
        "version : "+@version+"\n"+
        "description : "+@description+"\n"+
	"author : "+@author+"\n"+
        "tarball : "+@tarball+"\n"+
        "postinstall : "+@postinstall+"\n"+
        "kernel : "+@kernel+"\n"+
        "initrd : "+@initrd+"\n"+
        "hypervisor : "+@hypervisor+"\n"+
        "hypervisor_params : "+@hypervisor_params+"\n"+
        "fdisktype : "+@fdisktype+"\n"+
        "filesystem : "+@filesystem+"\n"+
        "environment_kind : "+@environment_kind+"\n"+
        "visibility : "+@visibility+"\n"+
        "demolishing_env : "+@demolishing_env+"\n"
  end
end

