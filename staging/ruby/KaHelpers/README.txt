This package bundles two helpers I use in my scripts
* Kaenv to manipulate Kadeploy3 environment descriptions
* KadeployWaiter to wait for the end of a deploiment

The rakefile has a few interesting targets:
*rake rdoc to generate documentation
*rake spec to test the classes 
*rake rcov to generate coverage reports 
*rake gem to generate a gem file

It can be installed as one of your gems using
  gem install KadeployWaiter-1.0.3.gem
