# Kaenv_spec.rb
require 'Kaenv'
require 'tempfile.rb'
require 'webmock/rspec'

RSpec.configure do |config|
  config.include WebMock::API
end

reference_description = ''
reference_description += 'name : Kaenv-rb-rspec'+"\n"
reference_description += 'version : 1'+"\n"
reference_description += 'description : stub environment to test Kaenv'+"\n"
reference_description += 'author : David.Margery@inria.fr'+"\n"
reference_description += 'tarball : /home/dmargery/OpenNebula/lenny-xen-chef.tgz|tgz'+"\n"
reference_description += 'postinstall : /grid5000/postinstalls/lenny-x64-xen-2.2-post.tgz|tgz|traitement.ash /rambin'+"\n"
reference_description += 'kernel : /boot/vmlinuz-2.6.26-2-xen-amd64'+"\n"
reference_description += 'initrd : /boot/initrd.img-2.6.26-2-xen-amd64'+"\n"
reference_description += 'hypervisor : /boot/xen-3.2-1-amd64.gz'+"\n"
reference_description += 'hypervisor_params : dom0_mem=1000000'+"\n"
reference_description += 'fdisktype : 83'+"\n"
reference_description += 'filesystem : ext3'+"\n"
reference_description += 'environment_kind : xen'+"\n"
reference_description += 'visibility : private'+"\n"
reference_description += 'demolishing_env : 0'+"\n"

describe Kaenv do
  before(:all) do  
    @env_file=Tempfile.new('Kaenv-rb-rspec')
    @env_file.write(reference_description)
    @env_file.close()
  end

  default_env_name="etch-x64-base"
  it "finds default environments such as #{default_env_name} and reads the contents correctly" do
    env=Kaenv.new(default_env_name)
    env.apiname.should == default_env_name
    env.name.should == default_env_name
  end
  
  it "it fetches environment descriptions from files" do
    env=Kaenv.new(@env_file.path)
    env.apiname.should == @env_file.path
    env.name.should == 'Kaenv-rb-rspec'
  end

  env_url='http://rspec.grid5000.fr/kaenv-rspec.env'
  it "It fetches remote environment descriptions such as #{env_url} and reads the contents correctly" do
    stub_http_request(:get, env_url).to_return(:body => reference_description)
    env=Kaenv.new(env_url)
    WebMock.should have_requested(:get, env_url).
      with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip, deflate'})
    env.apiname.should == env_url
    env.name.should == 'Kaenv-rb-rspec'
    env.to_s.should == reference_description
  end
  
  it "raises an exception when called with an unfound url" do
    stub_http_request(:get, env_url).to_return(:status => 404)
    lambda{Kaenv.new(env_rul)}.should raise_error 
    lambda{Kaenv.new(env_rul)}.should_not raise_error(NoMethodError) 
  end

  it "finds environments such as #{default_env_name} when deploy is explictly set as user" do
    env=Kaenv.new(default_env_name,'deploy')
    env.apiname.should == default_env_name
    env.name.should == default_env_name
  end

  garbage_string='0f334Qsdfgsdc'
  it "raises an exception when called with a garbage string such as #{garbage_string}" do
    lambda{Kaenv.new(garbage_sring)}.should raise_error 
    lambda{Kaenv.new(garbage_sring)}.should_not raise_error(NoMethodError) 
  end

  after(:all) do
    @env_file.unlink()
  end  
end
