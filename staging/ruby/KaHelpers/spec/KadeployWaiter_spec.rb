require "KadeployWaiter.rb"
require 'restclient'
require 'logger'


reference_json=''
success_json={:success => true }.to_json
describe KadeployWaiter do
  before(:all) do  
    if defined?(WebMock)
     WebMock.allow_net_connect!
    end
  end

  it "Finds an open port none is specified" do
    first_waiter=KadeployWaiter.new()
    first_waiter.s.config[:Port].should_not ==0
  end

  it "does something sensible if stop_port not defined" do
    first_waiter=KadeployWaiter.new(3456)
    first_waiter.url.should_not == "http://#{Socket.gethostname}:0"
  end

  it "Finds an open port one if a start_port is specified" do
    first_waiter=KadeployWaiter.new(4567)
    second_waiter=KadeployWaiter.new(4567)
    second_waiter.url.should_not == first_waiter.url
  end

  it "does something sensible if stop_port < start_port" do
    lambda{KadeployWaiter.new(3456,3455)}.should raise_error 
  end


  it "Raises an exception if it can't find an open port" do
    begin
      first_waiter=KadeployWaiter.new(4567,4567)
    rescue Errno::EADDRINUSE
    end
    lambda{KadeployWaiter.new(4567,4567)}.should raise_error 
  end

  it "Waits for a request on its reference url until timeout" do
    waiter=KadeployWaiter.new()
    result=waiter.waitEndDeployment(reference_json, 10)
    result.should == reference_json
  end

  it "Stops before timeout when it receives post on its url" do
    waiter=KadeployWaiter.new()
    x= Thread.new {
	sleep 5
    	waiter_service = RestClient::Resource.new(waiter.getUrl,:timeout => 5)
    	waiter_service.post(success_json)
    } 	
    result=waiter.waitEndDeployment(reference_json,10)
    x.join
    result.should == JSON.parse(success_json)
  end

  it "Starts and stops a server even when no timeout is sepcified" do
    waiter=KadeployWaiter.new()
    x= Thread.new {
	sleep 5
    	waiter_service = RestClient::Resource.new(waiter.getUrl,:timeout => 5)
    	waiter_service.post(success_json)
    } 	
    result=waiter.waitEndDeployment(reference_json)
    x.join
    result.should == JSON.parse(success_json)
  end

  after(:all) do  
    if defined?(WebMock)
     WebMock.disable_net_connect!
    end
  end
end


