SUBDIRS = admin staging bin

all: $(SUBDIRS)

clean: $(SUBDIRS)

.PHONY: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)




